
from pymongo import MongoClient


class DB:

    def __init__(self, config):
        host, port, db_name = config
        try:
            client = MongoClient(host=host, port=port)
            self.db = client.get_database(db_name)

        except:
            raise


    def get_collection(self, name):
        return self.db.get_collection(name=name) if self.db else None
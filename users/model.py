class Users:

    def __init__(self, db, collection_name = 'users'):
        self.collection = db.get_collection(collection_name)

    def find(self):
        pass
    def find_all(self, options={}):
        return self.collection.find(options)

    def create(self, options):
        return self.collection.insert_on(options).inserted_id

    def update(self):
        pass

    def delete(self):
        pass


    def exists(self, options):
        return bool(self.collection.find(options).count())


from flask import Blueprint
from flask import request

from utils.DB import DB
from config import Config
from .model import Users
import json


users = Blueprint('users', __name__, template_folder='templates')
@users.route('/', methods=['POST'])
def create():
    data = dict(json.loads(request.data.decode('utf-8')))
    model = get_model()
    errors = dict()

    if model.exists({'username': data.get('username')}):
        errors.update({'username': 'this user already exists'})

    if model.exists({'email': data.get('email')}):
        errors.update({'email': 'this email already used'})

    if len(errors):
        return json.dumps(errors), 403

    print(model.create(data))


    # print(list(model.find_all()))

    return json.dumps(data)


def get_model():
    db = DB(Config().get_db_config())
    return Users(db=db, collection_name='users')

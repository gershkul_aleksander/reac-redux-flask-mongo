from flask import Flask, render_template, url_for
from users.app import users
from config import Config

app = Flask(__name__)
app.register_blueprint(users, url_prefix='/api/users')



app.debug = Config.DEBUG

@app.route('/')
def hello_world():
    return render_template('layout.html')


@app.route('/login')
def login():
    return 'login'



if __name__ == '__main__':
    app.run()

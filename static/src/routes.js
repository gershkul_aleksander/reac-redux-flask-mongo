/**
 * Created by developer on 02.03.17.
 */
import React from 'react';
import { Route } from 'react-router';

import App from './components/App';
import { Users } from './components/Users';
import Dash from './components/Dash';
import User from './components/User';

import { SignIn } from './components/Users'
import SignUp from './contaners/SignUp'

const NoMatch = React.createClass({
    render: function(){
        return (
            <div>
                Empty
            </div>
        )
    }
})

export default (
    <Route path="/" component={App}>

            <Route path="sign-in" component={SignIn} />
            <Route path="sign-up" component={SignUp} />

            <Route path="dash" component={Dash} />
            <Route path="users" component={Users} />
            <Route path="/user/*" component={User} />
            <Route path="*" component={NoMatch} />
    </Route>
)

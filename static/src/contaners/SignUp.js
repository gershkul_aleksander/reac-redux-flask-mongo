/**
 * Created by developer on 06.03.17.
 */
import { connect } from 'react-redux'
import { signUp } from './../actions/users';
import { SignUp } from './../components/Users';

const mapStateToProps = (state) => {
    return {data: state.users.signUp.data}
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (elements) => {
            dispatch(signUp({
                'username': elements['username'].value,
                'email': elements['email'].value,
                'password': elements['password'].value,
                'confirmPassword': elements['confirmPassword'].value
            }));
            return false
        }
    }
};

const SignUpForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUp)

export default SignUpForm;

/**
 * Created by developer on 06.03.17.
 */
let Validator = {
    required: (val)=>{
        return /\S+/.test(val)
    },
    length: (val, min=0, max=64000)=>{
        let _val= val.replace(/\s/,'')
        return _val.length() >= min && _val.length() <= max;
    },
    email: (val)=>{
        return true
    },

};

export default Validator;
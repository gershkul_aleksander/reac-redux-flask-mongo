/**
 * Created by developer on 28.02.17.
 */

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, Redirect, browserHistory } from 'react-router';
import { syncHistoryWithStore} from 'react-router-redux';

import rApp from './reducers';
import thunkMiddleware from 'redux-thunk';
import routes from './routes';



const store = createStore(rApp, applyMiddleware(thunkMiddleware));
const history = syncHistoryWithStore(browserHistory, store)

render(
  <Provider store={store}>
    <Router history={history}>
        {/*<Redirect from="/" to="main"/>*/}
        {routes}
    </Router>
  </Provider>,
  document.getElementById('root')
);
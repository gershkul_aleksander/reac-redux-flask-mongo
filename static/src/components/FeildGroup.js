/**
 * Created by developer on 06.03.17.
 */
import React from 'react'
import { FormGroup, ControlLabel, FormControl, HelpBlock }  from 'react-bootstrap'

export const FieldGroup  = React.createClass({
    render: function(){
        return (
             <FormGroup controlId={this.props.name} validationState={this.props.validationState}>
                <ControlLabel>{this.props.label}</ControlLabel>
                <FormControl type={this.props.type} placeholder={this.props.placeholder}/>
                 {this.props.help && <HelpBlock>{this.props.help}</HelpBlock>}
             </FormGroup>
        )
    }
});


/**
 * Created by developer on 03.03.17.
 */
import React from 'react'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Glyphicon }  from 'react-bootstrap'

const MainMenu  =  React.createClass({
    render: function (){
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">React-Redux-Flask Proba</a>
                    </Navbar.Brand>
                <Navbar.Toggle />
                </Navbar.Header>
                    <Navbar.Collapse>
                    <Nav pullRight>
                        <NavItem eventKey={2} href="#">Link Right</NavItem>
                        <NavDropdown eventKey={1} title={<Glyphicon glyph="user" > Account</Glyphicon>} id="basic-nav-dropdown">
                            <MenuItem eventKey={2.1} href="sign-in">Sign in</MenuItem>
                            <MenuItem eventKey={2.2} href="sign-up">Sign up</MenuItem>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
});

export default MainMenu

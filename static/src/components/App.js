/**
 * Created by developer on 02.03.17.
 */
import React from 'react'
import Mainmenu from './Navbars'


const App  =  React.createClass({
    render: function(){
        return (
            <div className="container-fluid">
                <div className="row">
                    <Mainmenu/>
                </div>
                <div className="row">
                    the content
                    {this.props.children}
                </div>
                <div className="row">
                    the footer
                </div>
            </div>

        )
    }
})


export default App


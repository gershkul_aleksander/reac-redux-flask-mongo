/**
 * Created by developer on 02.03.17.
 */
import React from 'react'
import { Button, Form}  from 'react-bootstrap'
import { FieldGroup } from './FeildGroup'

export const Users  =  React.createClass({
    render: function(){
        return (
            <div>
                Users
            </div>
        )
    }
});

export const SignUp = React.createClass({
    render: function () {
        console.log(this.props);
        let has_errors =  this.props.data && this.props.data.errors;
        return (
           <form onSubmit={e => {
               e.preventDefault()
               this.props.onSubmit(e.currentTarget.elements);
           }}>
               <legend>Sign up form</legend>
               <FieldGroup name="username"  label="User Name"   type="text" placeholder="Enter user name"
                           validationState={ has_errors && this.props.data.errors.username ?  "error" : null}
                           help={ has_errors && this.props.data.errors.username ? this.props.data.errors.username : null}
               />
               <FieldGroup name="email"  label="Email" type="text"
                           validationState={ has_errors && this.props.data.errors.email ?  "error" : null}
                           help={ has_errors && this.props.data.errors.email ? this.props.data.errors.email : null}
               />
               <FieldGroup name="password"  label="Password"   type="password"
                           validationState={ has_errors && this.props.data.errors.password ?  "error" : null}
                           help={ has_errors && this.props.data.errors.password ? this.props.data.errors.password : null}
               />
               <FieldGroup name="confirmPassword"  label="Confirm password"  type="password"
                           validationState={ has_errors && this.props.data.errors.confirmPassword ?  "error" : null}
                           help={ has_errors && this.props.data.errors.confirmPassword ? this.props.data.errors.confirmPassword : null}
               />
               <Button type="submit" bsStyle="primary" > Submit  </Button>
           </form>
        )
    }
});

export const SignIn = React.createClass({
    render: function () {
        return (
            <div>
                the Sign in
            </div>
        )
    }
});

export default Users
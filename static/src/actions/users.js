/**
 * Created by developer on 06.03.17.
 */
import Validator from './../utils/validators'
import fetch from 'isomorphic-fetch'
import { USER_SIGN_UP } from './../consts'


export const signUp = (options) => {


    let errors = {};

    var result = {
        type: USER_SIGN_UP,
        data: options
    }


    if(!Validator.required(options['username'])){
        errors['username'] = "field username is required"
    }

    if(!Validator.required(options['email'])){
        errors['email'] = "field email is required"
    }

    if(!Validator.required(options['password'])){
        errors['password'] = "field password is required"
    }

    if(Object.keys(errors).length){
        return function (dispatch) {
             dispatch(signUpErrors({options: options, errors: errors}))
        }
    } else {
        return function (dispatch) {
            dispatch(signUpAction({options: options}))
        }
    }

    return result;

};


export const signUpErrors = (options) => {
    return {
        type: 'USER_SIGN_UP_ERROR',
        data: options
    }

};

export const signUpAction = (options) => {
        console.log(options.options)
     return function (dispatch) {
         return fetch('/api/users/',
             {
                 method: "POST",
                 body:  JSON.stringify(options.options)
             }).then(response => {
                 if(response.status == 200){
                     response.json().then(value => console.log(value))
                 }
                 if(response.status == 403){
                     response.json().then(
                         value => {
                          dispatch(signUpErrors({"errors": value}))
                         }
                     )
                 }
                }
             ).then(json => console.log(json))
     }


}

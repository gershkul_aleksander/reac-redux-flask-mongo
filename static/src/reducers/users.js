/**
 * Created by developer on 06.03.17.
 */
import { combineReducers } from 'redux'
import { USER_SIGN_UP } from './../consts'
const signUp = (state = {}, action) => {

    if(action.type == USER_SIGN_UP){
        return { 'data': action.data, 'state': 'created' }
    }

    if(action.type == 'USER_SIGN_UP_ERROR'){
        console.log(action)
        return {'data': action.data}
    }


    return state
};



export default combineReducers({
    signUp
})
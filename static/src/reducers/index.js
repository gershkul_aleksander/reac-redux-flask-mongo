/**
 * Created by developer on 02.03.17.
 */
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import app from './app'
import users from './users'

const rApp = combineReducers({
    routing: routerReducer,
    app,
    users
});

export default rApp;
